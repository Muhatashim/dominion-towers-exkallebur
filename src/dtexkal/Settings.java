package dtexkal;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/16/13
 * Time: 12:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Settings implements Serializable {
    private static Settings instance;
    private int mainAbilityIdx;
    private int magicAbilityIdx;

    public int getMainAbilityIdx() {
        return mainAbilityIdx;
    }

    public void setMainAbilityIdx(int mainAbilityIdx) {
        this.mainAbilityIdx = mainAbilityIdx;
    }

    public int getMagicAbilityIdx() {
        return magicAbilityIdx;
    }

    public void setMagicAbilityIdx(int magicAbilityIdx) {
        this.magicAbilityIdx = magicAbilityIdx;
    }

    public Settings() {
        Settings.instance = this;
    }

    public static Settings getInstance() {
        return instance;
    }

    public static void setInstance(Settings instance) {
        Settings.instance = instance;
    }
}
