package dtexkal.nodes;

import dtexkal.DominionTowerExKALLEBur;
import dtexkal.api.utils.Data;
import dtexkal.api.utils.RewardsBox;
import dtexkal.api.utils.Utils;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Timer;
import org.powerbot.game.api.wrappers.node.SceneObject;
import org.powerbot.game.api.wrappers.widget.Widget;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/11/13
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class Traveler extends Node {

    private Timer lastClickTimer = new Timer(2000);

    @Override
    public boolean activate() {
        return !DominionTowerExKALLEBur.get().fighter.activate();
    }

    @Override
    public void execute() {
        if (lastClickTimer.isRunning() && Players.getLocal().isMoving()) {
            return;
        }

        if (run())
            lastClickTimer.reset();
    }

    private boolean run() {
        for (int i = 0; i < 2; i++) {
            Widget w = Widgets.get(i == 0 ? 1173 : 1163);
            if (w != null && w.validate()) {
                WidgetChild wc = w.getChild(i == 0 ? 64 : 115);
                if (wc.visible()) {
                    wc.click(true);
                    return true;
                }
                return false;
            }
        }

        WidgetChild endurance = Widgets.get(1164, 23);
        if (endurance != null && endurance.validate())
            return endurance.click(true);

        SceneObject stairs = SceneEntities.getNearest(62678);
        if (stairs != null) {
            if (RewardsBox.getDominionFactor() > 0 || RewardsBox.isBoxOpen()) {
                RewardsBox.setOpenBox(true);

                if (RewardsBox.getRewardsAmount() + Inventory.getCount() > 28) {
                    RewardsBox.bankAll();

                    return true;
                }

                if (RewardsBox.contains(Data.SCORPION_MEAT))
                    RewardsBox.take(Data.SCORPION_MEAT);
                else if (RewardsBox.contains(Data.XP_BOOK))
                    RewardsBox.take(Data.XP_BOOK);
                else
                    RewardsBox.bankAll();

                if (RewardsBox.getRewardsAmount() == 0) {
                    return RewardsBox.setOpenBox(false);
                }
                return false;
            }

            if (stairs.isOnScreen()) {
                stairs.interact("Climb");
                return true;
            } else
                Utils.walk(stairs);
        }

        if (Players.getLocal().isMoving()) {
            return false;
        }

        SceneObject openDoors = SceneEntities.getNearest(Data.ENTRANCE_DOOR);
        SceneObject exitDoors = SceneEntities.getNearest(Data.EXIT_DOOR);

        if (exitDoors != null && openDoors != null) {
            if (!exitDoors.getLocation().canReach()) {
                if (!openDoors.getLocation().canReach())
                    if (exitDoors.isOnScreen())
                        exitDoors.interact("Go-through");
                    else
                        Utils.walk(exitDoors);
                else if (openDoors.isOnScreen())
                    openDoors.click(true);
                else
                    Utils.walk(openDoors);

                return true;
            }
        }
        if (openDoors != null && openDoors.getLocation().canReach()) {
            if (openDoors.isOnScreen())
                openDoors.click(true);
            else
                Utils.walk(openDoors);
            return true;
        }

        SceneObject upstairs = SceneEntities.getNearest(Data.NEXT_LEVEL);
        if (upstairs != null) {
            if (upstairs.isOnScreen())
                upstairs.interact("Climb-up");
            else Utils.walk(upstairs);
            return true;
        }

        return false;
    }
}
