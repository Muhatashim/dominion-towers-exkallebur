package dtexkal.nodes;

import dtexkal.Settings;
import dtexkal.api.bosses.Bosses;
import dtexkal.api.utils.combat.Combat;
import dtexkal.api.utils.combat.EOC;
import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.input.Keyboard;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Filter;
import org.powerbot.game.api.wrappers.node.Item;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/11/13
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Fighter extends Node {
    private static final Filter<Item> FOOD_FILTER = new Filter<Item>() {
        @Override
        public boolean accept(Item item) {
            for (String s : item.getWidgetChild().getActions())
                if (s != null && s.equals("Eat"))
                    return true;
            return false;
        }
    };
    public static int kills = 0;
    private Bosses currentlyFighting;

    @Override
    public boolean activate() {
        Bosses current = Bosses.getCurrent();
        if (currentlyFighting != null && current != null && !current.equals(currentlyFighting)) {
            currentlyFighting.getBoss().onFinish();
            kills++;
        }

        return (currentlyFighting = current) != null;
    }

    @Override
    public void execute() {
        if (Players.getLocal().getHealthPercent() <= 30) {
            Item food = Inventory.getItem(FOOD_FILTER);
            if (food != null) {
                food.getWidgetChild().interact("Eat");
            }
        }

        if (!currentlyFighting.getBoss().finished()) {
            currentlyFighting.getBoss().fight();
        }

        if (Combat.getAttacking() != null) {
            if (!EOC.canHotkey()) {
                WidgetChild x = Widgets.get(137, 4);
                if (x.visible())
                    x.click(true);
                else
                    Keyboard.sendText("", true);
            }
            if (!EOC.isBarUp()) {
                EOC.clickOpenBar();
            }

            if (Magic.autoCasting())
                EOC.setActionBarNumber(Settings.getInstance().getMagicAbilityIdx(), EOC.getActionBarNumber());
            else
                EOC.setActionBarNumber(Settings.getInstance().getMainAbilityIdx(), EOC.getActionBarNumber());

            EOC.activateAllGood();
        }
    }
}
