package dtexkal.nodes;

import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.tab.Equipment;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.Item;

import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/15/13
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class Reequipper extends Node {
    private final ItemSlot[] START_ITEMS;
    private final Vector<ItemSlot> changed = new Vector<>();

    public Reequipper() {

        Equipment.Slot[] slots = Equipment.Slot.values();
        START_ITEMS = new ItemSlot[slots.length];

        for (int i = 0; i < slots.length; i++) {
            Equipment.Slot slot = slots[i];
            START_ITEMS[i] = new ItemSlot(Equipment.getItem(slot), slot);
        }
    }

    @Override
    public boolean activate() {
        for (ItemSlot item : START_ITEMS) {
            boolean found = false;
            for (int i : Players.getLocal().getAppearance())
                if (item.getItem().getId() == i) {
                    found = true;
                }
            if (!found)
                changed.add(item);
        }
        return changed.size() > 0;
    }

    @Override
    public void execute() {
        for (int i = 0; i < START_ITEMS.length; i++) {
            ItemSlot curr = START_ITEMS[i];
            if (Equipment.getAppearanceId(curr.getSlot()) == -1) {
                Item item = Inventory.getItem(curr.getItem().getId());
                if (item != null) {
                    item.getWidgetChild().click(true);
                    Task.sleep(200, 500);
                }
            }
        }
    }

    private static class ItemSlot {
        private final Item item;
        private final Equipment.Slot slot;

        ItemSlot(Item item, Equipment.Slot slot) {
            this.item = item;
            this.slot = slot;
        }

        public Item getItem() {
            return item;
        }

        public Equipment.Slot getSlot() {
            return slot;
        }
    }
}
