package dtexkal.api.utils;

import org.powerbot.core.script.job.Task;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Filter;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.util.Timer;
import org.powerbot.game.api.wrappers.Locatable;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;


/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

    private static int nextTileDist = Random.nextInt(4, 7);

    public static boolean waitFor(Condition c, int waitTime) {
        Timer waiter = new Timer(waitTime);
        boolean activated;

        while (!(activated = c.validate()) && waiter.isRunning()) {
            Task.sleep(10);
        }

        return activated;
    }

    public static void doDropAction(Condition condition) {
        doDropAction(condition, new Filter<Item>() {
            @Override
            public boolean accept(Item item) {
                return true;
            }
        });
    }

    public static void doDropAction(Condition condition, Filter<Item> droppables) {
        Item[] items = Inventory.getItems(droppables);
        if (items.length == 0) {
            return;
        }

        Item item = items[0];
        int id = item.getId();

        for (String action : item.getWidgetChild().getActions()) {
            if (action != null && action.equals("Drop") || action.equals("Eat")) {
                item.getWidgetChild().interact("Drop");
            }
        }

        waitFor(condition, Random.nextInt(30000, 45000));

        GroundItem nearest = GroundItems.getNearest(id);
        if (nearest != null) {
            nearest.interact("Take");
        }
    }

    public static void walk(Locatable loc) {
        final Tile currDest = Walking.getDestination();
        if (!Players.getLocal().isMoving() || currDest != null && currDest.distanceTo() < nextTileDist) {
            Walking.walk(loc);

            waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return currDest != Walking.getDestination();
                }
            }, Random.nextInt(700, 1200));

            nextTileDist = Random.nextInt(4, 7);
        }
    }
}
