package dtexkal.api.utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class Data {
    public static final int[] EXIT_DOOR = {62686, 62687};
    public static final int[] ENTRANCE_DOOR = {62685, 62684};
    public static final int BANK_CHEST = 62691;
    public static final int REWARDS_BOX = 62688;
    public static final int NEXT_LEVEL = 62689;
    public static final int DOMINION_FACTOR_SETTING = 1317;
    public static final int XP_BOOK = 22340;
    public static final int SCORPION_MEAT = 22342;
}
