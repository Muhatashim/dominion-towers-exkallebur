package dtexkal.api.utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 10:00 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Condition {
    public boolean validate();
}
