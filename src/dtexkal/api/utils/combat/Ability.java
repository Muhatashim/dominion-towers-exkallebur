/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dtexkal.api.utils.combat;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.input.Keyboard;
import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

import java.awt.*;

/**
 * @author VOLT
 */
public enum Ability {

    SLICE(3, -1, 125, -1, 1, AbilityType.ATTACK),
    SLAUGHTER(30, -1, 250, 50, 7, AbilityType.ATTACK),
    OVERPOWER(30, -1, 400, 100, 10, AbilityType.ATTACK),
    HAVOC(10, -1, 100, -1, 4, AbilityType.ATTACK),
    BACKHAND(15, 3, 188, -1, 6, AbilityType.ATTACK),
    SMASH(10, -1, 125, -1, 5, AbilityType.ATTACK),
    BARGE(20, 3, 188, 50, 2, AbilityType.ATTACK),
    FLURRY(20, 6, 125, 50, 8, AbilityType.ATTACK),
    SEVER(30, 10, 188, -1, 3, AbilityType.ATTACK),
    HURRICANE(20, -1, 188, 50, 9, AbilityType.ATTACK),
    MASSACRE(60, -1, 100, 100, 11, AbilityType.ATTACK),
    METEOR_STRIKE(60, -1, 350, 100, 12, AbilityType.ATTACK),
    KICK(15, 3, 188, -1, 2, AbilityType.STRENGTH),
    PUNCH(5, -1, 100, -1, 3, AbilityType.STRENGTH),
    DISMEMBER(30, 6, 125, -1, 1, AbilityType.STRENGTH),
    FURY(20, 6, 150, -1, 4, AbilityType.STRENGTH),
    DESTROY(20, 6, 188, 50, 9, AbilityType.STRENGTH),
    QUAKE(20, -1, 100, 50, 8, AbilityType.STRENGTH),
    BERSERK(60, 20, 100, 100, 10, AbilityType.STRENGTH),
    CLEAVE(10, -1, 125, -1, 6, AbilityType.STRENGTH),
    ASSAULT(30, 6, 219, 50, 7, AbilityType.STRENGTH),
    SMITE(10, -1, 100, -1, 5, AbilityType.STRENGTH),
    PULVERISE(60, 25, 25, 100, 12, AbilityType.STRENGTH),
    FRENZY(60, 10, 20, 100, 11, AbilityType.STRENGTH),
    ANTICIPATION(24, 10, 10, -1, 1, AbilityType.DEFENSE),
    BASH(15, 3, 188, -1, 6, AbilityType.DEFENSE),
    REVENGE(20, 10, 10, 50, 9, AbilityType.DEFENSE),
    PROVOKE(10, 6, 50, -1, 3, AbilityType.DEFENSE),
    IMMORTALITY(120, 30, 25, 100, 12, AbilityType.DEFENSE),
    FREEDOM(30, 6, 100, -1, 2, AbilityType.DEFENSE),
    REFLECT(20, 10, 50, 50, 7, AbilityType.DEFENSE),
    RESONANCE(30, 6, 100, -1, 4, AbilityType.DEFENSE),
    REJUVENATE(60, 10, 40, 100, 11, AbilityType.DEFENSE),
    DEBILITATE(30, 10, 100, 50, 8, AbilityType.DEFENSE),
    PREPARATION(20, 10, 10, -1, 5, AbilityType.DEFENSE),
    BARRICADE(60, 10, 100, 100, 10, AbilityType.DEFENSE),
    MOMENTUM(-1, 1200, 50, 100, 7, AbilityType.CONSTITUTION),
    ASPHYXIATE(50, 7, AbilityType.MAGIC),
    WRACK(0, 1, AbilityType.MAGIC),
    OMNIPOWER(100, 12, AbilityType.MAGIC),
    DRAGON_BREATH(0, 6, AbilityType.MAGIC),
    IMPACT(0, 3, AbilityType.MAGIC),
    COMBUST(0, 5, AbilityType.MAGIC),
    SURGE(0, 2, AbilityType.MAGIC),
    DETONATE(50, 8, AbilityType.MAGIC),
    CHAIN(0, 4, AbilityType.MAGIC),
    WILD_MAGIC(50, 9, AbilityType.MAGIC),
    METAMORPHOSIS(100, 10, AbilityType.MAGIC),
    TSUNAMI(100, 11, AbilityType.MAGIC),
    PIERCING_SHOT(-1, 1, AbilityType.RANGE),
    SNAP_SHOT(50, 7, AbilityType.RANGE),
    DEADSHOT(100, 12, AbilityType.RANGE),
    SNIPE(-1, 4, AbilityType.RANGE),
    BINDING_SHOT(-1, 2, AbilityType.RANGE),
    FRAGMENTATION_SHOT(-1, 5, AbilityType.RANGE),
    ESCAPE(-1, 3, AbilityType.RANGE),
    RAPID_FIRE(50, 8, AbilityType.RANGE),
    RIOCHET(-1, 6, AbilityType.RANGE),
    BOMBARDMENT(50, 9, AbilityType.RANGE),
    INCENDIARY_SHOT(100, 10, AbilityType.RANGE),
    UNLOAD(100, 11, AbilityType.RANGE);
    private final static int QUICK_SELECT_PARENT = 640;
    private final int recoverTime;
    private final int lastingTime;
    private final int damage;
    private final int addrenNeeded;
    private final int widgetIdx;
    private final AbilityType type;

    Ability(int recoverTime, int lastingTime,
            int damage, int addrenNeeded, int widgetIdx, AbilityType type) {
        this.recoverTime = recoverTime;
        this.lastingTime = lastingTime;
        this.damage = damage;
        this.addrenNeeded = addrenNeeded;
        this.widgetIdx = widgetIdx;
        this.type = type;
    }

    Ability(int addrenNeeded, int widgetIdx, AbilityType type) {
        this.recoverTime = -1;
        this.lastingTime = -1;
        this.damage = -1;
        this.addrenNeeded = addrenNeeded;
        this.widgetIdx = widgetIdx;
        this.type = type;
    }

    public int getWidgetIdx() {
        return widgetIdx;
    }

    public int getTexId() {
        int idx = getWidgetIdx();
        switch (getType()) {
            default:
                return -1;
            case ATTACK:
                return 14206 + idx;
            case STRENGTH:
                return 14254 + idx;
            case DEFENSE:
                return 14218 + idx;
            case CONSTITUTION:
                return 14266 + idx;
            case MAGIC:
                return 14230 + idx;
            case RANGE:
                return 14242 + idx;
        }
    }

    public int getAddrenalineNeeded() {
        return addrenNeeded;
    }

    public boolean coolDownGood() {
        WidgetChild cooldownWidget = getCooldownWidget();
        return cooldownWidget != null && cooldownWidget.getTextureId() == 14521;
    }

    public boolean canUse() {
        return coolDownGood() && EOC.getEnergy() >= addrenNeeded
                && getQuickSelect() != null;
    }

    public WidgetChild getQuickSelect() {
        WidgetChild[] children = Widgets.get(QUICK_SELECT_PARENT).getChildren();
        for (WidgetChild child : children) {
            if (child.getTextureId() == getTexId()) {
                return child;
            }
        }
        return null;
    }

    public WidgetChild getCooldownWidget() {
        WidgetChild quickSelect = getQuickSelect();
        if (quickSelect == null) {
            return null;
        }
        switch (quickSelect.getIndex()) {
            default:
                return null;
            case 32:
                return Widgets.get(QUICK_SELECT_PARENT, 36);
            case 72:
                return Widgets.get(QUICK_SELECT_PARENT, 73);
            case 76:
                return Widgets.get(QUICK_SELECT_PARENT, 77);
            case 80:
                return Widgets.get(QUICK_SELECT_PARENT, 81);
            case 84:
                return Widgets.get(QUICK_SELECT_PARENT, 85);
            case 88:
                return Widgets.get(QUICK_SELECT_PARENT, 89);
            case 92:
                return Widgets.get(QUICK_SELECT_PARENT, 93);
            case 96:
                return Widgets.get(QUICK_SELECT_PARENT, 97);
            case 100:
                return Widgets.get(QUICK_SELECT_PARENT, 101);
            case 104:
                return Widgets.get(QUICK_SELECT_PARENT, 105);
            case 108:
                return Widgets.get(QUICK_SELECT_PARENT, 109);
            case 112:
                return Widgets.get(QUICK_SELECT_PARENT, 113);
        }
    }

    public char getHotkey() {
        WidgetChild quickSelect;
        if ((quickSelect = getQuickSelect()) == null) {
            return (char) -1;
        }
        int idx = quickSelect.getIndex();
        switch (idx) {
            default: {
                String text = Widgets.get(QUICK_SELECT_PARENT).getChild(idx + 3).getText();
                return text.length() > 0 ? text.charAt(0) : (char) -1;
            }
            case 32: {
                String text = Widgets.get(QUICK_SELECT_PARENT).getChild(70).getText();
                return text.length() > 0 ? text.charAt(0) : (char) -1;
            }
        }
    }

    public boolean use() {
        if (!canUse()) {
            return false;
        }

        if (!EOC.isBarUp()) {
            EOC.clickOpenBar();
            if (!Utils.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return EOC.isBarUp();
                }
            }, 2000)) {
                return false;
            }
        }
        char c = getHotkey();
        if (c != (char) -1) {
            if (!Character.isLetterOrDigit(c)) {
                Point p = getQuickSelect().getNextViewportPoint();
                Mouse.hop(p.x, p.y);
                Mouse.click(true);
                return true;
            }
            Keyboard.sendKey(c);
            return true;
        }
        return false;
    }

    public int getDamage() {
        return damage;
    }

    public AbilityType getType() {
        return type;
    }

    public int getRecoverTime() {
        return recoverTime * 1000;
    }

    public int getLastingTime() {
        return lastingTime * 1000;
    }

    public WidgetChild getWidget() {
        return Widgets.get(275, 16).getChild(getWidgetIdx());
    }
}
