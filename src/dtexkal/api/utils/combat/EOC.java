/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dtexkal.api.utils.combat;

import org.powerbot.core.script.job.Task;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

import java.util.*;

/**
 * @author VOLT
 */
public class EOC {

    public static HashMap<WidgetChild, Ability> getCurrentBindedAbilities() {
        WidgetChild[] children = Widgets.get(640).getChildren();
        HashMap<WidgetChild, Ability> abilities = new HashMap();
        for (WidgetChild child : children) {
            Ability[] values = Ability.values();
            for (Ability ability : values) {
                if (ability.getTexId() == child.getTextureId()) {
                    abilities.put(child, ability);
                    break;
                }
            }
        }
        return abilities;
    }

    public static void setActionBarNumber(int idx, int currentIdx) {
        int delta = idx - currentIdx;
        if (delta == 0) {
            return;
        }
        if (delta > 0) {
            for (int i = 0; i < delta; i++) {
                if (EOC.getActionBarNumber() == idx) {
                    return;
                }
                Widgets.get(640, 23).click(true);
                Task.sleep(400, 500);
            }
            Task.sleep(1500, 2000);
        } else {
            for (int i = 0; i > delta; i--) {
                if (EOC.getActionBarNumber() == idx) {
                    return;
                }
                Widgets.get(640, 24).click(true);
                Task.sleep(400, 500);
            }
            Task.sleep(1500, 2000);
        }
    }

    public static int getActionBarNumber() {
        return (Settings.get(682) & 0xF0) >> 5;
    }

    public static int getEnergy() {
        return (int) ((Widgets.get(640, 13).getWidth() / 425d) * 100);
    }

    public static Ability[] getAvailAbilities() {
        List<Ability> abilities = new ArrayList();
        for (Ability ability : Ability.values()) {
            if (ability.canUse() && ((int) ability.getHotkey()) != -1) {
                abilities.add(ability);
            }
        }
        Ability[] abilityArray = new Ability[abilities.size()];
        abilities.toArray(abilityArray);
        return abilityArray;
    }

    public static void sortAbilities(List<Ability> abilities) {
        Comparator org = new Comparator<Ability>() {
            @Override
            public int compare(Ability o1, Ability o2) {
                int diff = o2.getWidgetIdx() - o1.getWidgetIdx();
                if (diff == 0) {
                    return Random.nextInt(-1, 2);
                }
                return diff;
            }
        };
        Collections.sort(abilities, org);
    }

    public static void activateAllGood() {
        if (!EOC.isBarUp()) {
            EOC.clickOpenBar();
        }
        if (!canHotkey()) {
            return;
        }

        List<Ability> availAbilities = Arrays.asList(getAvailAbilities());
        sortAbilities(availAbilities);
        for (Ability ability : availAbilities) {
            ability.use();
            break;
        }
    }

    public static boolean canHotkey() {
        return Widgets.get(137, 56).getTextColor() == 0 && !Widgets.get(137, 2).visible();
    }

    public static boolean isBarUp() {
        return !Widgets.get(640, 120).visible();
    }

    public static void clickOpenBar() {
        WidgetChild up = Widgets.get(640, 120);
        if (up.visible()) {
            up.click(true);
        } else {
            Widgets.get(640, 11).click(true);
        }
    }
}
