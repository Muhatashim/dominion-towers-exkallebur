/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dtexkal.api.utils.combat;

import org.powerbot.core.script.job.Task;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Tabs;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.util.Filter;
import org.powerbot.game.api.wrappers.interactive.NPC;
import org.powerbot.game.api.wrappers.interactive.Player;
import org.powerbot.game.api.wrappers.widget.Widget;

/**
 * @author VOLT
 */
public class Combat {

    public static void setAttackIndex(int i) {
        if (i != -1) {
            if (!Tabs.ATTACK.isOpen()) {
                Tabs.ATTACK.open();
            }
            Widgets.get(884, i + 7).click(true);
            Task.sleep(1000);
        }
    }

    public static int getAttackSetting() {
        return Settings.get(627);
    }

    public static int getPlayerHp() {
        return (Settings.get(659) >>> 1);
    }

    public static void clickRetaliate() {
        if (!Tabs.ATTACK.isOpen()) {
            Tabs.ATTACK.open();
            Task.sleep(500);
        }
        Widget w = Widgets.get(464);
        if (w.validate()) {
            w.getChild(5).click(true);
            Task.sleep(500);
        }
    }

    public static boolean readyToFight() {
        return Players.getLocal().getInteracting() == null || !(Players.getLocal().getInteracting() instanceof NPC);
    }

    public static boolean isRetaliateOn() {
        return Settings.get(462) == 0;
    }

    public static void setRetaliate(boolean on) {
        boolean isOn = isRetaliateOn();
        if (!on && isOn) {
            clickRetaliate();
        } else if (on && !isOn) {
            clickRetaliate();
        }
    }

    public static int getPrayer() {
        return Integer.parseInt(Widgets.get(749, 6).getText());
    }

    public static Player[] getAllInteracting() {
        return Players.getLoaded(new Filter<Player>() {
            @Override
            public boolean accept(Player t) {
                try {
                    org.powerbot.game.api.wrappers.interactive.Character interacting = t.getInteracting();
                    return interacting != null && interacting.getName() != null && interacting.getName().equals(Players.getLocal().getName());
                } catch (Exception e) {
                }
                return false;
            }
        });
    }

    public static org.powerbot.game.api.wrappers.interactive.Character getAttacking() {
        try {
            return Players.getLocal().getInteracting();
        } catch (Exception e) {
            return null;
        }
    }

    public static Player getAttackingPlayer() {
        org.powerbot.game.api.wrappers.interactive.Character attacking = Players.getLocal().getInteracting();
        Player player = null;
        if (attacking instanceof Player) {
            player = (Player) attacking;
        }
        return player;
    }

    public static boolean inCombat() {
        return getAttacking() != null;
    }
}
