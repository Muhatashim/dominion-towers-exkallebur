package dtexkal.api.utils.combat.magic;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Tabs;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/13/13
 * Time: 3:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class Magic {

    private static final WidgetChild SPELLS_WIDGET = Widgets.get(275, 19);

    public static boolean autoCasting() {
        return Settings.get(0) != 0;
    }

    public static SPELLS[] getAvailables() {
        if (!Widgets.get(275, 16).validate()) {
            openTabs();
        }

        Vector<SPELLS> spells = new Vector<SPELLS>();
        for (WidgetChild widgetChild : Widgets.get(275, 16).getChildren()) {
            if (widgetChild.getTextColor() != 16777215)
                continue;
            for (SPELLS spells1 : SPELLS.values()) {
                if (spells1.toWidgetChild() == null) {
                    openTabs();
                }
                if (widgetChild.getAbsoluteLocation().distance(spells1.toWidgetChild().getAbsoluteLocation()) <= 5)
                    spells.add(spells1);
            }
        }
        return spells.toArray(new SPELLS[spells.size()]);
    }

    public static SPELLS getSelectedSpell() {
        for (SPELLS spell : SPELLS.values()) {
            if (spell.isSelected())
                return spell;
        }
        return null;
    }

    public static void openTabs() {
        Tabs.ABILITY_BOOK.open();

        WidgetChild spells = Widgets.get(275, 37);
        if (spells != null && spells.validate() && !spells.visible()) {
            spells.click(true);
        }
    }

    public static boolean activateBestSpell(SPELLS... spells) {
        for (SPELLS currCheckSpell : spells) {
            for (SPELLS spell : Magic.getAvailables()) {
                if (spell == currCheckSpell) {
                    spell.setSelected(true);
                    return true;
                }
            }
        }
        return false;
    }

    public static enum SPELLS {
        AIR_STRIKE(0, 29),
        WATER_STRIKE(1, 33),
        EARTH_STRIKE(2, 37),
        FIRE_STRIKE(3, 43),
        AIR_BOLT(4, 47),
        WATER_BOLT(5, 55),
        EARTH_BOLT(6, 61),
        FIRE_BOLT(7, 67),
        AIR_BLAST(8, 75),
        WATER_BLAST(9, 81),
        SLAVER_DART(10, 89),
        EARTH_BLAST(11, 93),
        FIRE_BLAST(12, 103),
        DIVINE_STORM(13, 109),
        AIR_WAVE(14, 117),
        WATER_WAVE(15, 123),
        EARTH_WAVE(16, 131),
        FIRE_WAVE(17, 137),
        STORM_OF_ARMADYL(18, 139),
        POLYPORE_STRIKE(19, 325),
        AIR_SURGE(20, 147),
        WATER_SURGE(21, 157),
        EARTH_SURGE(22, 153),
        FIRE_SURGE(23, 161);
        public static SPELLS[] AIR_SPELLS = new SPELLS[]{AIR_SURGE, AIR_WAVE, AIR_BLAST, AIR_BOLT, AIR_STRIKE};
        public static SPELLS[] WATER_SPELLS = new SPELLS[]{WATER_SURGE, WATER_WAVE, WATER_BLAST, WATER_BOLT, WATER_STRIKE};
        public static SPELLS[] EARTH_SPELLS = new SPELLS[]{EARTH_SURGE, EARTH_WAVE, EARTH_BLAST, EARTH_BOLT, EARTH_STRIKE};
        public static SPELLS[] FIRE_SPELLS = new SPELLS[]{FIRE_SURGE, FIRE_WAVE, FIRE_BLAST, FIRE_BOLT, FIRE_STRIKE};
        private int id;
        private int setting;

        private SPELLS(final int id, int setting) {
            this.id = id;
            this.setting = setting;

        }

        private SPELLS(final int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public int getSetting() {
            return setting;
        }

        public boolean isSelected() {
            return getSetting() == Settings.get(0);
        }

        public void setSelected(final boolean selected) {
            if (isSelected() != selected) {
                openTabs();
                WidgetChild spellWidget = toWidgetChild();
                Widgets.scroll(spellWidget, Widgets.get(275, 20));
                spellWidget.interact("Auto-cast");
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return isSelected() == selected;
                    }
                }, Random.nextInt(1000, 2000));
            }
        }

        @Deprecated
        public void activate() {
            setSelected(true);
        }

        public WidgetChild toWidgetChild() {
            return SPELLS_WIDGET.getChild(getId());
        }
    }
}
