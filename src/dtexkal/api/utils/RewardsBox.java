package dtexkal.api.utils;

import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.node.Item;
import org.powerbot.game.api.wrappers.node.SceneObject;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/15/13
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class RewardsBox {
    private static final int PARENT_WIDGET = 1171;

    public static boolean isBoxOpen() {
        return Widgets.get(PARENT_WIDGET).validate();
    }

    public static boolean setOpenBox(final boolean open) {
        if (isBoxOpen() == open) {
            return true;
        }

        if (open) {
            SceneObject rewards = SceneEntities.getNearest(Data.REWARDS_BOX);
            if (rewards != null) {
                if (rewards.isOnScreen()) {
                    rewards.click(true);
                    return Utils.waitFor(new Condition() {
                        @Override
                        public boolean validate() {
                            return isBoxOpen() == open;
                        }
                    }, Random.nextInt(2000, 3000));
                } else
                    Utils.walk(rewards);
            }
            return false;
        }

        WidgetChild x = Widgets.get(PARENT_WIDGET, 20);
        if (x.validate()) {
            return x.click(true);
        }
        return false;
    }

    public static int getRewardsAmount() {
        int amt = 0;
        for (WidgetChild widgetChild : Widgets.get(PARENT_WIDGET, 22).getChildren()) {
            if (widgetChild.getChildId() != -1) {
                Item item = new Item(widgetChild);
                amt += item.getStackSize();
            }
        }
        return amt;
    }

    public static boolean take(int id) {
        for (WidgetChild widgetChild : Widgets.get(PARENT_WIDGET, 22).getChildren()) {
            if (widgetChild.getChildId() == id)
                return widgetChild.click(true);
        }
        return false;
    }

    public static boolean contains(int id) {
        if (!isBoxOpen()) {
            setOpenBox(true);
            return false;
        }


        for (WidgetChild widgetChild : Widgets.get(PARENT_WIDGET, 22).getChildren()) {
            if (widgetChild.getChildId() == id)
                return true;
        }
        return false;
    }

    public static void bankAll() {
        if (!isBoxOpen()) {
            setOpenBox(true);
            return;
        }

        Widgets.get(PARENT_WIDGET, 37).click(true);
    }

    public static int getDominionFactor() {
        return Settings.get(Data.DOMINION_FACTOR_SETTING);
    }
}
