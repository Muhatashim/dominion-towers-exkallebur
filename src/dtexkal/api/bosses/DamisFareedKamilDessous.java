package dtexkal.api.bosses;

import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.wrappers.interactive.NPC;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/13/13
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class DamisFareedKamilDessous extends Boss {
    private static final String[] MONSTERS = {"Fareed", "Kamil", "Dessous"};

    public DamisFareedKamilDessous() {
        super("Fareed, Kamil and Dessous", null);
    }

    @Override
    public boolean activate() {
        return NPCs.getNearest(MONSTERS) != null;
    }

    @Override
    public void fight() {
        NPC nearest = NPCs.getNearest(MONSTERS);
        String name = nearest.getName();
        if (name.equals("Fareed"))
            Magic.activateBestSpell(Magic.SPELLS.WATER_SPELLS);
        else if (name.equals("Damis"))
            Magic.activateBestSpell(Magic.SPELLS.EARTH_SPELLS);
        else if (name.equals("Kamil"))
            Magic.activateBestSpell(Magic.SPELLS.FIRE_SPELLS);
        else if (name.equals("Dessous"))
            Magic.activateBestSpell(Magic.SPELLS.AIR_SPELLS);

        fight(nearest);
    }
}
