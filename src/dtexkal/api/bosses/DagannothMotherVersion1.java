package dtexkal.api.bosses;

import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.wrappers.interactive.NPC;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/13/13
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class DagannothMotherVersion1 extends Boss {

    public DagannothMotherVersion1() {
        super("Dagannoth Mother Version 1", "Dagannoth Mother");
    }

    @Override
    public void fight() {
        NPC npc = NPCs.getNearest(getMainMonsterName());

        if (Magic.getAvailables().length > 0) {
            switch (npc.getId()) {
                default:
                    if (Magic.autoCasting())
                        Magic.getSelectedSpell().setSelected(true);
                    break;
                case 14617: //brown
                    Magic.activateBestSpell(Magic.SPELLS.EARTH_SPELLS);
                    break;
                case 14614://white
                    Magic.activateBestSpell(Magic.SPELLS.AIR_SPELLS);
                    break;
                case 14615://blue
                    Magic.activateBestSpell(Magic.SPELLS.WATER_SPELLS);
                    break;
                case 14616://red
                    Magic.activateBestSpell(Magic.SPELLS.FIRE_SPELLS);
            }
        }

        fight(npc);
    }
}
