package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sigmund extends Boss {
    private final static int ANCIENT_MACE = 22406;

    public Sigmund() {
        super("Sigmund", "Sigmund");
    }

    @Override
    public void fight() {
        GroundItem nearest = GroundItems.getNearest(ANCIENT_MACE);
        if (nearest != null) {
            if (nearest.isOnScreen()) {
                nearest.interact("Take");

                if (Inventory.isFull()) {
                    Utils.doDropAction(new Condition() {
                        @Override
                        public boolean validate() {
                            return Inventory.containsAll(ANCIENT_MACE);
                        }
                    });
                }
            } else
                Utils.walk(nearest);
            return;
        }

        Item mace = Inventory.getItem(ANCIENT_MACE);
        if (mace != null) {
            mace.getWidgetChild().click(true);
            Utils.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return !Inventory.containsAll(ANCIENT_MACE);
                }
            }, 2000);
        }

        super.fight();
    }
}
