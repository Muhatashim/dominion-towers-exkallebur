package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Equipment;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.interactive.NPC;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/11/13
 * Time: 9:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class AgrithNaar extends Boss {
    private static final int SILVERLIGHT = 22404;
    private static int nextHealthChange = Random.nextInt(30, 40);

    public AgrithNaar() {
        super("Agrith Naar", "Agrith Naar");
    }

    @Override
    public void fight() {
        NPC npc = NPCs.getNearest(super.getMainMonsterName());
        if (npc.getHealthPercent() <= nextHealthChange && Equipment.getAppearanceId(Equipment.Slot.WEAPON) != SILVERLIGHT) {
            final Item silverLight = Inventory.getItem(SILVERLIGHT);
            if (silverLight != null) {
                silverLight.getWidgetChild().click(true);
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return !Inventory.containsAll(silverLight.getId());
                    }
                }, 2000);
            } else {
                GroundItem silverLightGItem = GroundItems.getNearest(SILVERLIGHT);
                if (silverLightGItem == null) {
                    return;
                }

                if (!silverLightGItem.isOnScreen()) {
                    Utils.walk(silverLightGItem);
                    return;
                }
                silverLightGItem.interact("Take");
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return Inventory.containsAll(silverLight.getId());
                    }
                }, 2000);
            }
            nextHealthChange = Random.nextInt(20, 30);
        }
        super.fight();
    }

}
