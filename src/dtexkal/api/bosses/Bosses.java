package dtexkal.api.bosses;

import dtexkal.api.utils.Data;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.interactive.NPC;
import org.powerbot.game.api.wrappers.node.SceneObject;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Bosses {
    SOLUS_DELLAGAR("Solus Dellagar"),
    BOUNCER("Bouncer"),
    KOSCHEI_THE_DEATHLESS("Koschei the Deathless"),
    ARRAV(new Arrav()),
    COUNT_DRAYNOR(new CountDraynor()),//TODO: test
    DAD("Dad"),
    GENERAL_KHAZARD("General Khazard"),
    TUMEKENS_SHADOW("Tumeken's Shadow"),
    JUNGLE_DEMON("Jungle Demon"),
    MELZAR_THE_MAD("Melzar the Mad"),
    DRAMEN_TREE_SPIRIT("Dramen Tree Spirit"),
    DAGANNOTH_MOTHER_VERSION_1(new DagannothMotherVersion1()), //TODO:test
    BARRELCHEST("Barrelchest"),
    EVIL_CHICKEN("Evil Chicken"),
    ARRG("Arrg"),
    TREUS_DAYTH("Treus Dayth"), //add special code for attacking pickaxes while in ghost mode (optional)
    AGRITH_NAAR(new AgrithNaar()),//TODO: test
    THE_UNTOUCHABLE("The Untouchable"),
    DECAYING_AVATAR("Decaying Avatar"),// needs spec code to work best | TODO: find roots id
    GIANT_SCARAB(new GiantScarab()), //TODO: test
    CULINAROMANCER("Culinaromancer"),
    ARZINIAN_AVATAR_MAGIC("Arzinian Avatar of Magic"),
    ARZINIAN_AVATAR_Range("Arzinian Avatar  of Ranging"),
    ARZINIAN_AVATAR_STRENGTH("Arzinian Avatar of Strength"),
    ICE_DEMON("Ice Demon"),// protect mage would be good
    THE_EVERLASTING(new TheEverLastingAndTheIllusive()),//needs special code The Illusive = 14524
    NOMAD("Nomad"),
    PEST_QUEEN(new PestQueen()),// TODO: test
    ERUNI_AYUNI_AND_LEEUNI(new EruniAyuniLeeuni()), //TODO: test
    DAGANNOTH_MOTHER("Dagannoth Mother"),
    ZENEVIVIA("Zenevivia"),
    DAMIS_FAREED_KAMIL_AND_DESSOUS(new DamisFareedKamilDessous()),//TODO: test
    THE_INADEQUACY_DOUBTS("The Inadequacy"), //may need special code
    GIANT_ROC("Giant Roc"),
    THE_BLACK_KNIGHT_TITAN("The Black Knight Titan"),
    THE_DRAUGEN("The Draugen"),
    KARAMEL(new Karamel()),
    BLACK_KNIGHT_GUARDIAN("Black Knight guardian"),
    NEZIKCHENED("Nezikchened"),
    BALANCE_ELEMENTAL("Balance Elemental"),
    SIGMUND(new Sigmund()),
    TARN_RANZORLOR(new TarnRanzorlor()),
    CHRONOZON(new Chronozon()),
    ELVARG("Elvarg"),
    DELRITH(new Delrith()),
    TOKTZ_KET_DILL(new TokTzKetDill()),//TODO: test
    THE_KENDAL("The Kendal"),
    NULL("-");
    private Boss boss;

    private Bosses(String name) {
        boss = new Boss(getName(), name) {
        };
    }

    private Bosses(Boss boss) {
        this.boss = boss;
    }

    public static Bosses getCurrent() {
        for (Bosses boss : values()) {
            if (boss.getBoss().activate()) {
                return boss;
            }
        }

        SceneObject entrance = SceneEntities.getNearest(Data.ENTRANCE_DOOR);
        SceneObject exit = SceneEntities.getNearest(Data.EXIT_DOOR);
        NPC nearest = NPCs.getNearest(NPCs.ALL_FILTER);
        if (entrance != null && exit != null && nearest != null) {
            Tile exL = exit.getLocation();
            Tile enL = entrance.getLocation();

            Tile avg = new Tile((exL.getX() + enL.getX()) / 2, (exL.getY() + enL.getY()) / 2, exL.getPlane());

            if (nearest.getLocation().distance(avg) <= 15) {
                NULL.setBoss(new Boss("Abstract Boss", nearest.getName()) {
                });
                return NULL;
            }
        }

        return null;
    }

    public Boss getBoss() {
        return boss;
    }

    public void setBoss(Boss boss) {
        this.boss = boss;
    }

    public String getName() {
        String name = name();
        return (name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()).replace('_', ' ');
    }
}
