package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Equipment;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/15/13
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class Delrith extends Boss {
    private final static int SILVERLIGHT = 22404;

    public Delrith() {
        super("Delrith", "Delrith");
    }

    @Override
    public void fight() {
        if (Equipment.getAppearanceId(Equipment.Slot.WEAPON) != SILVERLIGHT) {
            final Item silverLight = Inventory.getItem(SILVERLIGHT);
            if (silverLight != null) {
                silverLight.getWidgetChild().click(true);
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return !Inventory.containsAll(silverLight.getId());
                    }
                }, 2000);
            } else {
                GroundItem silverLightGItem = GroundItems.getNearest(SILVERLIGHT);
                if (silverLightGItem == null) {
                    return;
                }

                if (!silverLightGItem.isOnScreen()) {
                    Utils.walk(silverLightGItem);
                    return;
                }
                silverLightGItem.interact("Take");
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return Inventory.containsAll(silverLight.getId());
                    }
                }, 2000);
            }
        }
        super.fight();
    }
}
