package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/16/13
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class TokTzKetDill extends Boss {
    public TokTzKetDill() {
        super("TokTz-Ket-Dill", "TokTz-Ket-Dill");
    }

    private final static int PICKAXE = 22407;

    @Override
    public void fight() {
        Item pick = Inventory.getItem(PICKAXE);
        if (pick != null) {
            pick.getWidgetChild().click(true);
            Utils.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return Inventory.containsAll(PICKAXE);
                }
            }, Random.nextInt(1500, 2000));
        } else {
            GroundItem nearest = GroundItems.getNearest(PICKAXE);
            if (nearest != null) {
                if (nearest.isOnScreen())
                    nearest.interact("Take");
                else
                    Walking.walk(nearest);
                return;
            }
        }

        super.fight();
    }
}
