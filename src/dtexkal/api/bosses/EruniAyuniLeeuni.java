package dtexkal.api.bosses;

import org.powerbot.game.api.methods.interactive.NPCs;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/13/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class EruniAyuniLeeuni extends Boss {
    private static final String[] MONSTERS = {"Eruni", "Ayuni", "Leeuni"};

    public EruniAyuniLeeuni() {
        super("Eruni, Ayuni and Leeuni", null);
    }

    @Override
    public boolean activate() {
        return NPCs.getNearest(MONSTERS) != null;
    }

    @Override
    public void fight() {
        JOptionPane.showMessageDialog(null, "Fighting " + this.getClass().getName());
        fight(NPCs.getNearest(MONSTERS));
    }
}
