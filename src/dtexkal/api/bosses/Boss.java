package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import dtexkal.api.utils.combat.Combat;
import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.core.event.events.MessageEvent;
import org.powerbot.core.event.listeners.MessageListener;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.methods.widget.Camera;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.interactive.NPC;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 9:53 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Boss implements MessageListener {
    private String bossType;
    private String mainMonsterName;

    public Boss() {
    }

    public Boss(String bossType, String mainMonsterName) {
        this.bossType = bossType;
        this.mainMonsterName = mainMonsterName;
    }

    @Override
    public void messageReceived(MessageEvent messageEvent) {
    }

    public String getBossType() {
        return bossType;
    }

    public String getMainMonsterName() {
        return mainMonsterName;
    }

    public boolean finished() {
        return !Widgets.get(1159).validate();
    }

    public void fight(NPC npc) {
        if (!Combat.readyToFight() || finished() || npc == null) {
            return;
        }

        Tile randomized = npc.getLocation().randomize(1, 1, 1, 1);
        if (npc == null || !randomized.canReach()) {
            return;
        }

        if (!npc.isOnScreen()) {
            Utils.walk(randomized);
            Camera.turnTo(npc);
            Camera.setPitch(0);
            return;
        }

        npc.interact("Attack");
        Utils.waitFor(new Condition() {
            @Override
            public boolean validate() {
                return !Combat.readyToFight();
            }
        }, 2000);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Boss boss = (Boss) o;

        if (bossType != null ? !bossType.equals(boss.bossType) : boss.bossType != null) return false;
        if (mainMonsterName != null ? !mainMonsterName.equals(boss.mainMonsterName) : boss.mainMonsterName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bossType != null ? bossType.hashCode() : 0;
        result = 31 * result + (mainMonsterName != null ? mainMonsterName.hashCode() : 0);
        return result;
    }

    public void onFinish() {
        if (Magic.autoCasting()) {
            Magic.getSelectedSpell().setSelected(false);
        }

    }

    public void fight() {
        assert mainMonsterName != null;
        fight(NPCs.getNearest(mainMonsterName));
    }

    @Override
    public String toString() {
        return "dtexkal.api.bosses.Boss{" +
                "bossType='" + bossType + '\'' +
                ", mainMonsterName='" + mainMonsterName + '\'' +
                '}';
    }

    public boolean activate() {
        assert mainMonsterName != null;
        return NPCs.getNearest(mainMonsterName) != null;
    }
}
