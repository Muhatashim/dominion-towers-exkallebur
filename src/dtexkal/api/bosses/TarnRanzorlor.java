package dtexkal.api.bosses;

import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.wrappers.interactive.NPC;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class TarnRanzorlor extends Boss {
    private final static int TARN = 14430;

    public TarnRanzorlor() {
        super("TarnRanzorlor", "Mutant tarn");
    }

    @Override
    public void fight() {
        NPC tarn = NPCs.getNearest(TARN);
        if (tarn != null) {
            fight(tarn);
            return;
        }

        super.fight();
    }
}
