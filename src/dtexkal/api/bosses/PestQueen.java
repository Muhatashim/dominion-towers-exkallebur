package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.interactive.Character;
import org.powerbot.game.api.wrappers.interactive.NPC;
import org.powerbot.game.api.wrappers.interactive.Player;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 8:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class PestQueen extends Boss {
    private final int DRONE = 12286;

    public PestQueen() {
        super("Pest Queen", "Pest Queen");
    }

    @Override
    public void fight() {
        for (NPC drone : NPCs.getLoaded(DRONE)) {
            final Character interacting = drone.getInteracting();
            if (interacting != null && interacting instanceof NPC) {
                interacting.interact("Attack");
                Utils.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return interacting.getInteracting() instanceof Player;
                    }
                }, Random.nextInt(1500, 2000));
                return;
            }
        }

        super.fight(NPCs.getNearest(getMainMonsterName()));
    }
}
