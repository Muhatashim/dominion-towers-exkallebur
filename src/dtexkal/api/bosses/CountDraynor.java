package dtexkal.api.bosses;

import dtexkal.api.utils.Condition;
import dtexkal.api.utils.Utils;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Filter;
import org.powerbot.game.api.wrappers.node.GroundItem;
import org.powerbot.game.api.wrappers.node.Item;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/10/13
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class CountDraynor extends Boss {
    private final static int STAKE_HAMMER = 22403;
    private final static int STAKE = 22402;

    public CountDraynor() {
        super("Count Draynor", "Count Draynor");
    }

    @Override
    public void fight() {
        if (Inventory.containsAll(STAKE, STAKE_HAMMER))
            super.fight();
        else {
            final GroundItem next = GroundItems.getNearest(-1, STAKE_HAMMER, STAKE);

            System.out.println(GroundItems.getNearest(STAKE_HAMMER));

            if (next == null)
                return;

            if (!next.isOnScreen()) {
                Utils.walk(next);
                return;
            }

            if (Inventory.isFull()) {
                Utils.doDropAction(new Condition() {
                                       @Override
                                       public boolean validate() {
                                           next.interact("Take");
                                           return Inventory.containsAll(next.getId());
                                       }
                                   }, new Filter<Item>() {
                                       @Override
                                       public boolean accept(Item item) {
                                           int id = item.getId();
                                           return id != STAKE_HAMMER && id != STAKE;
                                       }
                                   }
                );
                return;
            }

            next.interact("Take");
        }
    }
}
