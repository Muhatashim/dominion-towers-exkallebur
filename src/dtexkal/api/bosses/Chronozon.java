package dtexkal.api.bosses;

import dtexkal.api.utils.Utils;
import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.core.event.events.MessageEvent;
import org.powerbot.core.event.listeners.MessageListener;
import org.powerbot.game.api.methods.node.GroundItems;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.GroundItem;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class Chronozon extends Boss implements MessageListener {
    private final int[] ELEMENTALS = {12850, 12851};
    private final Magic.SPELLS[] BLASTS = {Magic.SPELLS.AIR_BLAST, Magic.SPELLS.WATER_BLAST, Magic.SPELLS.FIRE_BLAST, Magic.SPELLS.EARTH_BLAST};

    public Chronozon() {
        super("Chronozon", "Chronozon");
    }

    private int blastIdx = 0;

    @Override
    public void fight() {
        GroundItem nearest = GroundItems.getNearest(ELEMENTALS);
        if (nearest != null) {
            if (nearest.isOnScreen())
                nearest.interact("Take");
            else
                Utils.walk(nearest);
            return;
        }

        if (!Inventory.containsAll(ELEMENTALS)) {
            if (Magic.autoCasting())
                Magic.getSelectedSpell().setSelected(false);
        } else
            Magic.activateBestSpell(BLASTS[blastIdx]);

        super.fight();
    }


    @Override
    public void messageReceived(MessageEvent messageEvent) {
        if (messageEvent.getId() == 0 && messageEvent.getMessage().contains("seems to have done"))
            if (blastIdx < BLASTS.length)
                blastIdx++;
            else
                blastIdx = 0;
    }
}
