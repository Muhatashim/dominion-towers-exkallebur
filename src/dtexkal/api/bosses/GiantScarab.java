package dtexkal.api.bosses;

import dtexkal.api.utils.combat.magic.Magic;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/13/13
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class GiantScarab extends Boss {
    public GiantScarab() {
        super("Giant Scarab", "Giant Scarab");
    }

    @Override
    public void fight() {
        if (Magic.getAvailables().length > 0) {
            if (!Magic.activateBestSpell(Magic.SPELLS.SLAVER_DART))
                Magic.activateBestSpell(Magic.SPELLS.EARTH_SPELLS);
        }

        super.fight();
    }
}
