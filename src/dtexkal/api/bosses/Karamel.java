package dtexkal.api.bosses;

import dtexkal.api.utils.combat.magic.Magic;
import org.powerbot.game.api.methods.interactive.NPCs;
import org.powerbot.game.api.wrappers.interactive.NPC;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 8:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class Karamel extends Boss {
    public Karamel() {
        super("Karamel", "Karamel");
    }

    private final static String MOTHER = "Gelatinnoth Mother";

    @Override
    public void fight() {
        if (NPCs.getNearest(getMainMonsterName()) != null) {
            super.fight();
            return;
        }

        NPC mother = NPCs.getNearest(MOTHER);
        if (mother != null) {
            switch (mother.getId()) {
                default:
                    if (Magic.autoCasting())
                        Magic.getSelectedSpell().setSelected(false);
                    break;
                case 14481: //brown
                    Magic.activateBestSpell(Magic.SPELLS.EARTH_SPELLS);
                    break;
                case 14479: //red
                    Magic.activateBestSpell(Magic.SPELLS.FIRE_SPELLS);
                    break;
                case 14476: //white
                    Magic.activateBestSpell(Magic.SPELLS.AIR_SPELLS);
                    break;
                case 14478: //blue
                    Magic.activateBestSpell(Magic.SPELLS.WATER_SPELLS);
                    break;
            }

            fight(mother);
        }
    }

    @Override
    public boolean activate() {
        return NPCs.getNearest(MOTHER, getMainMonsterName()) != null;
    }
}
