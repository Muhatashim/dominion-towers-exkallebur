package dtexkal.api.bosses;

import org.powerbot.game.api.methods.interactive.NPCs;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/14/13
 * Time: 7:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TheEverLastingAndTheIllusive extends Boss {
    private final int THE_ILLUSIVE = 14524;

    public TheEverLastingAndTheIllusive() {
        super("The Everlasting and The Illusive", "The Everlasting");
    }

    @Override
    public boolean activate() {
        return NPCs.getNearest(super.getMainMonsterName()) != null || NPCs.getNearest(THE_ILLUSIVE) != null;
    }

    @Override
    public void fight() {
        if (NPCs.getNearest(super.getMainMonsterName()) == null) {
            super.fight(NPCs.getNearest(THE_ILLUSIVE));
        }

        super.fight();
    }
}
