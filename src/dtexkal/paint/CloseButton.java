package dtexkal.paint;

import dtexkal.DominionTowerExKALLEBur;
import dtexkal.paint.wrappers.Button;
import dtexkal.paint.wrappers.TabPane;
import dtexkal.paint.wrappers.Utils;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/23/13
 * Time: 3:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class CloseButton extends Button {
    private static Image closeButton = Utils.getImage("http://icons.iconarchive.com/icons/dakirby309/windows-8-metro/32/Other-Power-Log-Off-Metro-icon.png");

    public CloseButton(int x, int y) {
        super(closeButton, x, y);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x, y, closeButton.getWidth(null), closeButton.getHeight(null));
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TabPane pane = DominionTowerExKALLEBur.get().getPainter().getPane();
        pane.setVisible(!pane.isVisible());
    }
}
