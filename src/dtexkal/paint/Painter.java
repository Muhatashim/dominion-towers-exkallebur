package dtexkal.paint;

import dtexkal.paint.tabs.Main;
import dtexkal.paint.tabs.Stats;
import dtexkal.paint.wrappers.Tab;
import dtexkal.paint.wrappers.TabPane;
import dtexkal.paint.wrappers.Utils;
import org.powerbot.game.api.methods.input.Mouse;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class Painter {
    private final Font font;
    private final Color fontColor;
    private TabPane pane;
    private Point[] mousePath = new Point[20];
    private int mpIdx = 0;

    public Painter() {
        pane = new TabPane(476, 415, Utils.getImage("http://lh6.googleusercontent.com/-iqKqziKdI8U/USkTmPZocdI/AAAAAAAAAHk/ktzKAIivXPE/s505/Comp+1+%280-00-00-00%29.png"));
        pane.addTab(new Main());
        pane.addTab(new Stats());

        pane.setOpenTab(pane.getTabs().get(0));

        font = new Font("Segoe UI", 0, 12);
        fontColor = new Color(0x3A3A33);
    }

    public TabPane getPane() {
        return pane;
    }

    public void draw(Graphics grphcs) {

        Graphics2D g = (Graphics2D) grphcs;

        if (pane.isVisible()) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g.setFont(font);
            g.setColor(fontColor);
            pane.paint(g);

            drawMouse(g);
        }
    }

    public void onClick(MouseEvent me) {
        if (pane.getBounds().contains(me.getPoint())) {
            pane.mouseClicked(me);
            return;
        }
        for (Tab tab : pane.getTabs()) {
            if (tab.getBounds().contains(me.getPoint())) {
                tab.mouseClicked(me);
                return;
            }
        }
    }

    private int manhattanDist(Point p, Point p2) {
        return Math.abs(p2.x - p.x) + Math.abs(p2.y - p.y);
    }

    public void drawMouse(Graphics2D g) {
        if (mpIdx >= mousePath.length)
            mpIdx = 0;
        mousePath[mpIdx++] = Mouse.getLocation();

        Point p1, p2;
        int distance, conns;
        for (int i = 0; i < mousePath.length; i++) {
            p1 = mousePath[i];
            if (p1 == null) {
                break;
            }
            conns = 0;
            for (int i2 = 0; i2 < mousePath.length; i2++) {
                p2 = mousePath[i2];
                if (p2 == null || conns >= 5) {
                    break;
                }
                if (p1.equals(p2))
                    continue;

                distance = manhattanDist(p1, p2);
                if (distance <= 100) {
                    g.setColor(new Color(252, 252, 252, distance));
                    g.drawLine(p1.x, p1.y, p2.x, p2.y);
                    conns++;
                }
            }
        }
    }
}
