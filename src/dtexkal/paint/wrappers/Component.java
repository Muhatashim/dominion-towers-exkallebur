package dtexkal.paint.wrappers;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:39 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Component implements MouseListener {
    protected final int x;
    protected final int y;
    protected boolean visible;

    public Component(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public final void paint(Graphics g) {
        if (visible)
            draw(g);
    }

    public abstract Rectangle getBounds();

    protected abstract void draw(Graphics g);

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
