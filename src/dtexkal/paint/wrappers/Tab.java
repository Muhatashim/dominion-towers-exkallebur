package dtexkal.paint.wrappers;


import dtexkal.DominionTowerExKALLEBur;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class Tab extends Component implements Runnable {
    private final Image icon;
    private final List<Component> components;

    public Tab(Image ico) {
        this(ico, 7, 395);
    }

    public Tab(Image icon, int x, int y) {
        super(x, y);
        this.icon = icon;
        this.components = new ArrayList<>();
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    public Image getIcon() {
        return icon;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TabPane pane = DominionTowerExKALLEBur.get().getPainter().getPane();
        pane.setVisible(!pane.isVisible());
    }

    @Override
    public Rectangle getBounds() {
        Rectangle best = new Rectangle(x, y, 469, 127);
        for (Component component : components)
            if (component.getBounds() != null) {
                Rectangle bounds = component.getBounds();
                if (!best.contains(bounds)) {
                    best = best.union(bounds);
                }
            }
        return best;
    }

    public void draw(Graphics g) {
        for (Component component : components)
            component.paint(g);
    }

    @Override
    public void run() {
    }
}
