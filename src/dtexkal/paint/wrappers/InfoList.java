package dtexkal.paint.wrappers;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class InfoList extends Component {

    private final int x;
    private final int y;
    private Label[] text;

    public InfoList(Label labels[], int x, int y) {
        super(x, y);
        this.text = labels;
        this.x = x;
        this.y = y;
    }

    @Override
    public Rectangle getBounds() {
        return null;
    }

    @Override
    protected void draw(Graphics g) {
        int currY = y;
        for (Label l : text) {
            g.drawString(l.toString(), x, currY += g.getFontMetrics().getHeight());
        }
    }

    public void setLabels(Label[] labels) {
        this.text = labels;
    }
}
