package dtexkal.paint.wrappers;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 7:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class Label {
    private String text;
    private String info;

    public Label(String text, String info) {
        this.text = text;
        this.info = info;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return getText() + getInfo();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setText(String text, String info) {
        this.text = text;
        this.info = info;
    }
}
