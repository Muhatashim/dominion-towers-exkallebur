package dtexkal.paint.wrappers;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/23/13
 * Time: 3:05 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Button extends Component {
    private final Image bg;

    public Button(Image bg, int x, int y) {
        super(x, y);
        this.bg = bg;
    }

    @Override
    public abstract void mouseClicked(MouseEvent e);

    @Override
    protected void draw(Graphics g) {
        g.drawImage(bg, x, y, null);
    }
}
