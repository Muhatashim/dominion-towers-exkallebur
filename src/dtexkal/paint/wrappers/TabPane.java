package dtexkal.paint.wrappers;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class TabPane extends Component {
    private final static int IMAGE_ICON_LENGTH = 30;
    private final List<Tab> tabs;
    private final Image bgImage;
    private final Image closeButton;
    private Tab openTab;

    public TabPane(int x, int y, Image bgImage) {
        super(x, y);
        this.bgImage = bgImage;
        closeButton = Utils.getImage("http://icons.iconarchive.com/icons/dakirby309/windows-8-metro/32/Other-Power-Log-Off-Metro-icon.png");
        tabs = new ArrayList();
        openTab = null;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int nextY = y;
        for (Tab tab : tabs)
            if (tab.getIcon() != null) {
                Rectangle bounds = new Rectangle(x, nextY, IMAGE_ICON_LENGTH, IMAGE_ICON_LENGTH);
                nextY += 5 + IMAGE_ICON_LENGTH;
                if (bounds.contains(e.getPoint())) {
                    switchTab(tab);
                    return;
                }
            }
    }

    private void switchTab(Tab tab) {
        getOpenTab().setVisible(false);
        tab.setVisible(true);
        openTab = tab;
    }

    @Override
    public Rectangle getBounds() {
        Rectangle best = new Rectangle(x, y, 0, 0);
        int nextY = y;
        for (Tab tab : tabs)
            if (tab.getIcon() != null) {
                Rectangle bounds = new Rectangle(x, nextY, IMAGE_ICON_LENGTH, IMAGE_ICON_LENGTH);
                nextY += 5 + IMAGE_ICON_LENGTH;
                best = best.union(bounds);
            }
        return best;
    }

    public void draw(Graphics g) {
        Tab openTab = getOpenTab();
        g.drawImage(bgImage, openTab.x, openTab.y, null);
        openTab.paint(g);

        int size = tabs.size();
        int nextY = y;
        for (int i = 0; i < size; i++) {
            Tab tab = tabs.get(i);
            tab.run();
            Image icon = tab.getIcon();
            if (icon != null) {
                Image scaled = icon.getScaledInstance(IMAGE_ICON_LENGTH, IMAGE_ICON_LENGTH, 0);
                g.drawImage(scaled, x, nextY, null);
                nextY += IMAGE_ICON_LENGTH + 5;
            }
        }
    }

    public void addTab(Tab tab) {
        setOpenTab(tab);
        tabs.add(tab);
    }

    public Tab getOpenTab() {
        return openTab;
    }

    public void setOpenTab(Tab openTab) {
        this.openTab = openTab;
    }

    public List<Tab> getTabs() {
        return tabs;
    }
}
