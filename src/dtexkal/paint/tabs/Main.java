package dtexkal.paint.tabs;

import dtexkal.DominionTowerExKALLEBur;
import dtexkal.api.bosses.Bosses;
import dtexkal.api.utils.RewardsBox;
import dtexkal.paint.wrappers.InfoList;
import dtexkal.paint.wrappers.Label;
import dtexkal.paint.wrappers.Tab;
import dtexkal.paint.wrappers.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class Main extends Tab {

    private Label[] labels;

    public Main() {
        super(Utils.getImage("http://icons.iconarchive.com/icons/double-j-design/origami-colored-pencil/48/red-home-icon.png"));
        labels = new Label[]{
                new Label("Runtime: ", null),
                new Label("Current DF: ", null),
                new Label("Kills: ", null),
                new Label("Kills per hour: ", null),
                new Label("Monster: ", null),
                new Label("State: ", null),
        };
        addComponent(new InfoList(labels, 145, 415));
    }

    public void setState(String state) {
        labels[4].setInfo(state);
    }

    @Override
    public void run() {
        int i = 0;
        labels[i++].setInfo(DominionTowerExKALLEBur.get().runtime.toElapsedString());
        labels[i++].setInfo(String.valueOf(RewardsBox.getDominionFactor()));
        labels[i++].setInfo(String.valueOf(DominionTowerExKALLEBur.get().fighter.kills));
        labels[i++].setInfo(String.valueOf((float) DominionTowerExKALLEBur.get().fighter.kills / (DominionTowerExKALLEBur.get().runtime.getElapsed() * 1000 * 60 * 60)));
        Bosses currentMonster = Bosses.getCurrent();
        labels[i++].setInfo((currentMonster != null) ? currentMonster.getName() : "none");
    }
}
