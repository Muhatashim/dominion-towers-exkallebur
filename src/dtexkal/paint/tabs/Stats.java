package dtexkal.paint.tabs;

import dtexkal.paint.wrappers.InfoList;
import dtexkal.paint.wrappers.Label;
import dtexkal.paint.wrappers.Tab;
import dtexkal.paint.wrappers.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/18/13
 * Time: 6:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class Stats extends Tab {

    private Label[] labels;

    public Stats() {
        super(Utils.getImage("http://icons.iconarchive.com/icons/pixelmixer/basic/32/statistics-icon.png"));
        labels = new Label[]{
                new Label("Tab coming soon.", "")
        };
        addComponent(new InfoList(labels, 145, 415));
    }


    @Override
    public void run() {
    }
}
