package dtexkal;

import dtexkal.api.bosses.Boss;
import dtexkal.api.bosses.Bosses;
import dtexkal.nodes.Fighter;
import dtexkal.nodes.Prayerer;
import dtexkal.nodes.Reequipper;
import dtexkal.nodes.Traveler;
import dtexkal.paint.Painter;
import org.powerbot.core.event.events.MessageEvent;
import org.powerbot.core.event.listeners.MessageListener;
import org.powerbot.core.event.listeners.PaintListener;
import org.powerbot.core.script.ActiveScript;
import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.Manifest;
import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.util.Timer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 2/11/13
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Manifest(
        authors = "ExKALLEBur",
        name = "Dominion Tower ExKALLEBur",
        website = "http://www.powerbot.org/community/topic/920131-dominion-towers-exkallebur/",
        description = "Owns the crap out of dominion towers.",
        topic = 923365,
        version = 1.121)

public class DominionTowerExKALLEBur extends ActiveScript implements MessageListener, PaintListener, MouseListener {
    private static DominionTowerExKALLEBur instance;
    public final Traveler traveler;
    public final Fighter fighter;
    public final Prayerer prayerer;
    public final Reequipper reequipper;
    private final Node[] NODES;
    public Timer runtime;
    private boolean started = false;
    private GUI gui;
    private Painter painter;

    public DominionTowerExKALLEBur() {
        NODES = new Node[]{fighter = new Fighter(), traveler = new Traveler(), prayerer = new Prayerer(), reequipper = new Reequipper()};

        runtime = new Timer(0);
        painter = new Painter();
        instance = this;
    }

    public static DominionTowerExKALLEBur get() {
        return instance;
    }

    public Painter getPainter() {
        return painter;
    }

    @Override
    public void onStart() {
        JOptionPane.showMessageDialog(null, "Welcome to the beta release version of Dominion Tower ExKALLEBur!\nThe script only supports endurance mode atm and 95% of the bosses are coded.\nThe other 5% is done with basic virtual intelligence. Please report all bugs on my thread; thanks.");
        JOptionPane.showMessageDialog(null, "Please have all your runes you want to use in your inventory and your main attack weapon already wielded along with your armour now.");
        Mouse.setSpeed(Mouse.Speed.FAST);

        new Settings();

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    gui = new GUI();
                    gui.setVisible(true);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        while (gui.isVisible())
            Task.sleep(100);

        started = true;
    }

    @Override
    public int loop() {
        if (!started) {
            return 100;
        }

        for (Node node : NODES)
            try {
                if (node.activate())
                    node.execute();
            } catch (Exception e) {
            }
        return 10;
    }

    @Override
    public void messageReceived(MessageEvent messageEvent) {
        for (Bosses bosses : Bosses.values()) {
            Boss boss = bosses.getBoss();
            if (boss != null)
                boss.messageReceived(messageEvent);
        }
    }

    @Override
    public void onRepaint(Graphics graphics) {
        painter.draw(graphics);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        painter.onClick(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
